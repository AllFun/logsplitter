package com.edu.own.customsplitter;

import ch.qos.logback.core.Appender;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import ch.qos.logback.core.spi.AppenderAttachable;
import ch.qos.logback.core.spi.AppenderAttachableImpl;

import java.util.Iterator;
import java.util.List;

public abstract class CustomAppenderBase<E> extends UnsynchronizedAppenderBase<E> implements AppenderAttachable<E> {

    private final AppenderAttachableImpl<E> attachable = new AppenderAttachableImpl<>();

    protected abstract List<E> split(E e);

    protected abstract boolean isSplitRequired(E e);

    @Override
    protected void append(E e) {
        if (isSplitRequired(e)) {
            split(e).forEach(attachable::appendLoopOnAppenders);
        } else {
            attachable.appendLoopOnAppenders(e);
        }
    }

    @Override
    public void addAppender(final Appender<E> appender) {
        addInfo("Adding appender: [" + appender.getName() + "] to " + getName());
        attachable.addAppender(appender);
    }

    @Override
    public Iterator<Appender<E>> iteratorForAppenders() {
        return attachable.iteratorForAppenders();
    }

    @Override
    public Appender<E> getAppender(final String appenderName) {
        return attachable.getAppender(appenderName);
    }

    @Override
    public boolean isAttached(final Appender<E> appender) {
        return attachable.isAttached(appender);
    }

    @Override
    public void detachAndStopAllAppenders() {
        attachable.detachAndStopAllAppenders();
    }

    @Override
    public boolean detachAppender(final Appender<E> appender) {
        return attachable.detachAppender(appender);
    }

    @Override
    public boolean detachAppender(final String appenderName) {
        return attachable.detachAppender(appenderName);
    }

    @Override
    public abstract String getName();
}
