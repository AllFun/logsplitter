package com.edu.own.customsplitter;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.stream.IntStream;

import static com.edu.own.customsplitter.LoggingEventUtil.splitString;
import static java.lang.String.valueOf;
import static java.util.stream.Collectors.toList;

@Getter
@Setter
public class CustomLengthSplittingAppender extends CustomAppenderBase<ILoggingEvent> {

    private int maxLength;
    private String sequenceKey;

    @Override
    protected List<ILoggingEvent> split(final ILoggingEvent iLoggingEvent) {
        final List<String> messageParts = splitString(iLoggingEvent.getFormattedMessage(), getMaxLength());
        final IntFunction<ILoggingEvent> processEventPart =
                index -> cloneEventPart(index, messageParts.get(index), iLoggingEvent);

        return IntStream.range(0, messageParts.size())
                .mapToObj(processEventPart)
                .collect(toList());
    }

    private ILoggingEvent cloneEventPart(final int index, final String messagePart, final ILoggingEvent baseEvent) {
        final LoggingEvent clonedEvent = LoggingEventUtil.clone(baseEvent);
        final Map<String, String> mdcPropertyMap = new HashMap<>(baseEvent.getMDCPropertyMap());
        mdcPropertyMap.put(getSequenceKey(), valueOf(index));
        clonedEvent.setMDCPropertyMap(mdcPropertyMap);
        clonedEvent.setMessage(messagePart);

        return clonedEvent;
    }

    @Override
    protected boolean isSplitRequired(final ILoggingEvent iLoggingEvent) {
        return iLoggingEvent.getFormattedMessage().length() > maxLength;
    }

    @Override
    public String getName() {
        return this.getClass().getName();
    }

}
