package com.edu.own.customsplitter;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.LoggingEvent;
import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.lang.String.format;
import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

@UtilityClass
public class LoggingEventUtil {

    public static final LoggingEvent clone(final ILoggingEvent event) {
        final LoggingEvent clonedEvent = new LoggingEvent();

        clonedEvent.setLevel(event.getLevel());
        clonedEvent.setLoggerName(event.getLoggerName());
        clonedEvent.setTimeStamp(event.getTimeStamp());
        clonedEvent.setLoggerContextRemoteView(event.getLoggerContextVO());
        clonedEvent.setThreadName(event.getThreadName());
        ofNullable(event.getMarker())
                .ifPresent(clonedEvent::setMarker);
        ofNullable(event.getCallerData())
                .ifPresent(clonedEvent::setCallerData);

        return clonedEvent;
    }

    public static List<String> splitStringAdv(final String content, final int charLimit) {
        return new ArrayList<>(
                Arrays.asList(content.split(format("(?<=\\G.{%d})", charLimit)))
        );
    }

    public static List<String> splitString(final String content, final int charLimit) {
        if (charLimit == 0) {
            return singletonList(content);
        }
        final ArrayList<String> parts = new ArrayList<>((content.length() + charLimit - 1) / charLimit);

        for (int startIndex = 0; startIndex < content.length(); startIndex += charLimit) {
            parts.add(content.substring(startIndex, Math.min(content.length(), startIndex + charLimit)));
        }

        return parts;
    }
}
