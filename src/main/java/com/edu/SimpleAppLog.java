package com.edu;

import lombok.extern.slf4j.Slf4j;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

@Slf4j
public class SimpleAppLog {
    public static void main(String[] args) {
        log.info(IntStream.range(0, 500).mapToObj(String::valueOf).collect(joining()));
    }
}
