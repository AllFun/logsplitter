package com.edu.own.customsplitter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.function.IntFunction;
import java.util.stream.IntStream;

import static com.edu.own.customsplitter.LoggingEventUtil.splitString;
import static com.edu.own.customsplitter.LoggingEventUtil.splitStringAdv;
import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

class LoggingEventUtilTest {

    @Test
    void shouldSplit() {
        final String content = IntStream.rangeClosed(1, 100).mapToObj(String::valueOf).collect(joining());
        final int limit = 15;

        assertThat(splitString(content, limit))
                .containsExactlyElementsOf(splitStringAdv(content, limit))
                .containsExactly(
                        "123456789101112",
                        "131415161718192",
                        "021222324252627",
                        "282930313233343",
                        "536373839404142",
                        "434445464748495",
                        "051525354555657",
                        "585960616263646",
                        "566676869707172",
                        "737475767778798",
                        "081828384858687",
                        "888990919293949",
                        "596979899100"
                );
    }

    @Test
    void shouldSplitAll() {
        final String content = IntStream.range(0, 100).mapToObj(String::valueOf).collect(joining());
        final IntFunction<Executable> map = limit ->
                () -> assertThat(splitString(content, limit))
                        .isEqualTo(splitStringAdv(content, limit));

        assertAll(IntStream.range(0, 100).mapToObj(map));
    }
}