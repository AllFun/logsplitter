package com.edu;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

@UtilityClass
public class TestContextUtils {

    @SneakyThrows
    public static void setupLogback(final String propertyPath) {
        final LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        final JoranConfigurator configurator = new JoranConfigurator();
        configurator.setContext(context);
        context.reset();
        final InputStream configResource = TestContextUtils.class.getClassLoader().getResourceAsStream(propertyPath);
        configurator.doConfigure(configResource);
    }
}
