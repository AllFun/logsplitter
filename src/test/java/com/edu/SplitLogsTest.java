package com.edu;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class SplitLogsTest {

    @Test
    void shouldLog() {
        log.info(IntStream.rangeClosed(0, 100).mapToObj(String::valueOf).collect(joining()));
        assertThat(TestAppender.getEvents())
                .containsExactlyInAnyOrder(
                        "0 012345678910111213141516171819202122232425262728",
                        "1 293031323334353637383940414243444546474849505152",
                        "2 535455565758596061626364656667686970717273747576",
                        "3 777879808182838485868788899091929394959697989910",
                        "4 0"
                );

    }
}
