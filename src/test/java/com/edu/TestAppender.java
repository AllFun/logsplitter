package com.edu;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class TestAppender extends AppenderBase<ILoggingEvent> {

    @Getter
    private static final List<String> events = new ArrayList<>();

    @Override
    protected void append(final ILoggingEvent iLoggingEvent) {
        events.add(iLoggingEvent.getMDCPropertyMap().get("seq") + " " + iLoggingEvent.getFormattedMessage());
    }
}
